package com.yader.examengapsi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.yader.examengapsi.adapter.adapter;
import com.yader.examengapsi.model.datos;
import com.yader.examengapsi.model.records;
import com.yader.examengapsi.service.ApiClient;
import com.yader.examengapsi.service.ApiService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button buscar;
    EditText etBuscar;
    RecyclerView rvProductos;
    ArrayList<records> productoList ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buscar = findViewById(R.id.btnBuscar);
        buscar.setOnClickListener(this);
        etBuscar = findViewById(R.id.etBuscar);
        rvProductos = findViewById(R.id.rvProductos);
        rvProductos.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == buscar.getId()){
            if (etBuscar!=null && !etBuscar.getText().toString().isEmpty()) {
                llamadoWS();
            }
            else{
                Toast.makeText(getApplicationContext(),"Ingrese su busqueda",Toast.LENGTH_LONG).show();
            }
        }
    }

    public void llamadoWS(){
        String search;
        search = etBuscar.getText().toString();
        final ApiService apiService = ApiClient.getAPIService();
        final Call<datos> productos = apiService.getProductos(true,search,1,5);
        productos.enqueue(new Callback<datos>() {
            @Override
            public void onResponse(Call<datos> call, Response<datos> response) {
                datos products =  response.body();
                if (response.isSuccessful() &&  (products != null)){

                    productoList = new ArrayList<>();
                    int lista = products.getPlpResults().getRecords().size();
                    List<records> rec =products.getPlpResults().getRecords();
                    for (records reco: rec){
                        productoList.add(new records(reco.getSmImage(),reco.getName(),reco.getListPrice()));
                    }
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                        rvProductos.setLayoutManager(linearLayoutManager);
                        adapter listAdapterA = new adapter(productoList);
                        rvProductos.setAdapter(listAdapterA);

                        Log.d("consumo", "El consumo fue correcto en onResponse");

                }
                else{
                    Log.d("consumo", "El consumo fue incorrecto en onResponse");
                }
            }

            @Override
            public void onFailure(Call<datos> call, Throwable t) {
                Log.d("consumo", "El consumo fue incorrecto en onFailure");
            }
        });
    }

}
