package com.yader.examengapsi.model;

public class records {
    String smImage;
    String productDisplayName;
    String listPrice;

    public records(String smImage, String productDisplayName, String listPrice) {
        this.smImage = smImage;
        this.productDisplayName = productDisplayName;
        this.listPrice = listPrice;
    }

    public String getSmImage() {
        return smImage;
    }

    public void setSmImage(String smImage) {
        this.smImage = smImage;
    }

    public String getName() {
        return productDisplayName;
    }

    public void setName(String name) {
        this.productDisplayName = name;
    }

    public String getListPrice() {
        return listPrice;
    }

    public void setListPrice(String listPrice) {
        this.listPrice = listPrice;
    }
}
