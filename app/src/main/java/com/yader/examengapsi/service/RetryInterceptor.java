package com.yader.examengapsi.service;

import android.util.Log;

import java.io.IOException;
import java.net.SocketException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

class RetryInterceptor implements Interceptor {
    private int reintentos;

    public RetryInterceptor(int reintentos) {
        this.reintentos = reintentos;
    }


    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = null;
        boolean responseOK = false;
        int tryCount = 0;

        while (!responseOK && tryCount < this.reintentos) {
            try {
                Log.d("RetryInterceptor", "Intentando Request - " + tryCount);
                response = chain.proceed(request);
                responseOK = response.isSuccessful() | (response.code() >= 400 && response.code() <= 499) ;
            }catch (Exception e){
                Log.d("RetryInterceptor", "Request sin exito - " + tryCount);
                Log.d("RetryInterceptor", e.toString());
                Log.d("RetryInterceptor", e.getStackTrace().toString());

            }finally{
                tryCount++;
            }
        }

        if (response == null) {
            throw new SocketException("Error no especificado de red");
        }

        // otherwise just pass the original response on
        return response;
    }
}
