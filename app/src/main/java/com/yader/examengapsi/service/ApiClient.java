package com.yader.examengapsi.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {
    //public static final boolean SHOWROOM = BuildConfig.showroom;

    private static String BASEURL;
    private static Retrofit getRetrofitInstance()
    {


        BASEURL = "https://shoppapp.liverpool.com.mx/";

        Gson gson = new GsonBuilder().setLenient().serializeNulls().create();

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        RetryInterceptor retryInterceptor = new RetryInterceptor(5);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10000, TimeUnit.MILLISECONDS)
                .readTimeout(20000, TimeUnit.MILLISECONDS)
                .writeTimeout(20000, TimeUnit.MILLISECONDS)
                .addInterceptor(loggingInterceptor)
                .addInterceptor(retryInterceptor)
                .build();


        return new Retrofit.Builder().baseUrl(BASEURL).client(okHttpClient).addConverterFactory(GsonConverterFactory.create(gson)).build();


    }

    public static ApiService getAPIService()
    {
        return getRetrofitInstance().create(ApiService.class);
    }

}
