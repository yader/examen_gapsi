package com.yader.examengapsi.service;
import com.yader.examengapsi.model.datos;
import com.yader.examengapsi.model.records;

import retrofit2.Call;
import retrofit2.http.GET;

import retrofit2.http.Query;

public interface ApiService {
    @GET("appclienteservices/services/v3/plp?")
    Call<datos> getProductos(@Query("force-plp") Boolean force_plp,
                             @Query("search-string") String search_string,
                             @Query("page-number") int page_number,
                             @Query("number-of-items-per-page") int number_of_items_per_page);
}
