package com.yader.examengapsi.adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.yader.examengapsi.R;
import com.yader.examengapsi.model.records;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class adapter extends RecyclerView.Adapter<adapter.ViewHolder>{
   ArrayList<records> productos;
    Bitmap b;
    public adapter(ArrayList<records> productos) {
        this.productos = productos;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_producto, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Picasso.get().load(productos.get(position).getSmImage()).into(holder.ivProducto);
        //holder.ivProducto.setImageBitmap(b);
        holder.tvTitulo.setText(productos.get(position).getName());
        holder.tvPrecio.setText(productos.get(position).getListPrice());
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivProducto;
        TextView tvTitulo;
        TextView tvPrecio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivProducto = itemView.findViewById(R.id.ivProducto);
            tvTitulo = itemView.findViewById(R.id.tvTitulo);
            tvPrecio = itemView.findViewById(R.id.tvPrecio);
        }
    }
}
